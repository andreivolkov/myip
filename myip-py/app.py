# A simple python web service which outputs a client's IP address, by Andrei Volkov.
# The service is written for a specific usage case (a Kubernetes docker pod behind a Nginx Ingress Controller),
# and might not function correctly under a different environment.
# Specifically, a non-standard HTTP header HTTP_X_REAL_IP should be present (typically set by proxy & balancer services).
# Also an Ingress Controller should be configured correctly to preserve a client's IP address (externalTrafficPolicy should be set to Local).

from flask import Flask, request, jsonify

app = Flask(__name__)

# Container health checks
@app.route('/health')
def home():
    return jsonify("I'm alive")

@app.route('/')
def show_client_ip():
    ip_addr = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)   
    return 'Your IP is: ' + ip_addr + \
        '<br><br>Demo app by Andrei Volkov'

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False, port=8080, use_reloader=False)